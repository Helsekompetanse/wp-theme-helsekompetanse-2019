**Theme Name:** Helsekompetanse 2019
**Theme URI:** https://helsekompetanse.no
**License:** GPLv2 or later
**License URI:** http://www.gnu.org/licenses/gpl-2.0.html


# Helsekompetanse 2019


Helsekompetanse's version of the Wordpress theme Twenty Nineteen. Hence the theme name "Helsekompetanse 2019".

A thought on naming convention: another theme that e.g. implements Bootstrap 4 could be called "Helsekompetanse Bootstrap 4"?.


## Install

1. Git clone this (requires that you have Git installed).

2. For coding (S)CSS (requires that you have Node installed -- preferably through [Homebrew](https://brew.sh/)). Open this project folder in a terminal and run:

   - `npm install`


## NPM kommandoer

_Dette var ikke dokumentert i det orginalet 'themet' -- tolket av @Johan fra "package.json" fila._

For å kompilere "scss" -> "css" ved hver endring:
- `npm run watch`

For å kompilere "scss" -> "css" én gang:
- `npm run build`

Utdrag fra package.json:
```json
{
  "build:style": "node-sass style.scss style.css --output-style expanded && postcss -r style.css",
  "build:style-editor": "node-sass style-editor.scss style-editor.css --output-style expanded && postcss -r style-editor.css",
  "build:style-editor-customizer": "node-sass style-editor-customizer.scss style-editor-customizer.css --output-style expanded && postcss -r style-editor-customizer.css",
  "build:rtl": "rtlcss style.css style-rtl.css",
  "build:print": "node-sass print.scss print.css --output-style expanded && postcss -r print.css",
  "build": "run-p \"build:*\"",
  "watch": "chokidar \"**/*.scss\" -c \"npm run build\" --initial"
}
```

## Language

Norwegian Bokmål language files are included in the theme. They are translated by using the [Loco plugin][loco] on a local instance of Wordpress (or any instance where you can 'git push' to origin).

[loco]: https://wordpress.org/plugins/loco-translate/


## History

When transfering the code from the Twenty Nineteen theme the following steps where followed:

1. When converting Twenty Nineteen to a Helsekompetanse theme, the following strings were replaced:

   - Twenty Nineteen -> Helsekompetanse 2019
   - Twenty_Nineteen -> Helsekompetanse_2019
   - twentynineteen -> helsekompetanse2019

2. ...in these filetypes (not .css, .map -- I considered those to be compiled from buildtools):

   - scss, php, json, js (excluding the "node_modules" folder).

   __E.g. use (Neo)vim with these commands:__

   ```sh
   nvim ./*.scss sass/*.scss ./*.php classes/**/*.php inc/**/*.php template-parts/**/*.php ./*.json js/**/*.js
   :bufdo %s/Twenty Nineteen/Helsekompetanse 2019/ge | update
   :bufdo %s/Twenty_Nineteen/Helsekompetanse_2019/ge | update
   :bufdo %s/twentynineteen/helsekompetanse2019/ge | update
   ```

3. And both files in the "classes" folder were renamed to include the string "helsekompetanse2019" instead of "twentynineteen".

## Original copyright text of Twenty Nineteen

Twenty Nineteen WordPress Theme, Copyright 2018 WordPress.org
Twenty Nineteen is distributed under the terms of the GNU GPL.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

Twenty Nineteen bundles the following third-party resources:

_s, Copyright 2015-2018 Automattic, Inc.
**License:** GPLv2 or later
Source: https://github.com/Automattic/_s/

normalize.css, Copyright 2012-2016 Nicolas Gallagher and Jonathan Neal
**License:** MIT
Source: https://necolas.github.io/normalize.css/
