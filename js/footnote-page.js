/**
 * File footnote-page.js.
 *
 * Shows "back" link and adds functionality for that.
 */
( function() {

	var siteBranding = document.querySelector('.site-branding[hidden]');

	siteBranding.removeAttribute('hidden');

	document.getElementById('go-to-prev-page').onclick = function() {
		window.history.back();
		return false;
	};

} )();
