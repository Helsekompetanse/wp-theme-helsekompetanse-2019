/*
 * Enhancement.js.
 *
 * - Turns cover blocks with 1 link to a big 'link' (makes the entire block
 *   clickable).
 */

(function() {
	function createCoverLinks () {
		var covers = document.querySelectorAll('.wp-block-cover, .wp-block-cover-image');

		Array.prototype.forEach.call(covers, function(el) {
			var anchor = el.querySelectorAll('a');

			// If block only has 1 link, make whole block clickable
			if (anchor !== null && anchor.length === 1) {
				el.classList.add('cover-link');
				el.onclick = function openHeadingLink() {
					window.location.href = anchor[0].getAttribute('href');
				};
			}
		});
	}

	// After window load
	window.addEventListener('load', function() {
		createCoverLinks();
	})
})();
