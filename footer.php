<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<?php get_template_part( 'template-parts/footer/footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
