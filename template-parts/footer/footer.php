<?php
/**
 * The template part for displaying the site footer
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.8.6
 */

?>

	<footer id="colophon" class="site-footer">
		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
		<?php get_template_part( 'template-parts/footer/footer', 'site-info' ); ?>
	</footer><!-- #colophon -->
