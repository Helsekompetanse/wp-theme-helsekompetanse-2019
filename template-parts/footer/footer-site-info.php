<?php
/**
 * Displays the footer site info
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 2.1.1
 */

if ( has_nav_menu( 'footer' ) ) : ?>
	<div class="site-info">
			<nav class="footer-navigation" aria-label="<?php esc_attr_e( 'Footer Menu', 'helsekompetanse2019' ); ?>">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'footer',
						'menu_class'     => 'footer-menu',
						'depth'          => 1,
					)
				);
				?>
			</nav><!-- .footer-navigation -->
	</div><!-- .site-info -->
<?php endif; ?>
