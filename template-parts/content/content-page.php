<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php if ( ! helsekompetanse2019_can_show_post_thumbnail() && the_title('','',false) != '') : ?>
	<header class="entry-header">
		<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
	</header>
	<?php endif; ?>

	<?php if ( has_nav_menu( 'local' ) || is_active_sidebar( 'sidebar-2' ) ) : ?>
	<aside class="sidebar" aria-label="<?php esc_attr_e( 'Navigation and/or widgets.', 'helsekompetanse2019' ); ?>">
		<?php if ( has_nav_menu( 'local' ) ) : ?>
			<nav id="local-navigation" class="local-navigation" aria-label="<?php esc_attr_e('Contextual navigation', 'helsekompetanse2019'); ?>" hidden>
		<?php
		wp_nav_menu(
			array(
				'theme_location' => 'local',
				'menu_class'		 => 'local-menu',
				'items_wrap'		 => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			)
		);
		?>
			</nav><!-- #site-navigation -->
			<?php endif; ?>

		<?php get_template_part( 'template-parts/content/content', 'widgets' ); ?>
	</aside>
	<?php endif; ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'helsekompetanse2019' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php if ( get_edit_post_link() ) : ?>
		<?php
		edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'helsekompetanse2019' ),
						array(
							'span' => array(
								'class' => array(),
								),
							)
						),
					get_the_title()
					),
				'<span class="edit-link">',
				'</span>'
				);
		?>
		<?php endif; ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
