<?php
/**
 * Displays page next and prev nav
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 2.0.1
 */
?>
<nav id="page-next-prev-navigation" class="navigation page-navigation" role="navigation" hidden>
	<span class="prev-next-page">
		<a href="#" id="next-page-link" class="next-page button" hidden>
			<span class="help-text"><?php _e('Next', 'helsekompetanse2019'); ?><span class="colon">:</span></span>
			<span id="next-page-text" class="link-title"></span>
			<span aria-hidden="true" class="help-text icon">→</span>
		</a>

		<a href="#" id="prev-page-link" class="prev-page button-link" hidden>
			<span aria-hidden="true" class="help-text icon">←</span>
			<span class="help-text"><?php _e('Previous page', 'helsekompetanse2019'); ?></span>
		</a>
	</span>
</nav>
