<?php
/**
 * Displays the content (<main>) widget area
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.4.0
 */

if ( is_active_sidebar( 'sidebar-2' ) ) : ?>

	<div class="widget-area local-position" role="complementary" aria-label="<?php esc_attr_e( 'Widgets', 'helsekompetanse2019' ); ?>">
		<?php dynamic_sidebar( 'sidebar-2' ); ?>
	</div><!-- .widget-area -->

<?php endif; ?>
