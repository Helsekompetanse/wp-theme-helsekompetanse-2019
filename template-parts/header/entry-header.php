<?php
/**
 * Displays the post header
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.0.0
 */

$discussion = helsekompetanse2019_can_show_post_thumbnail() ? helsekompetanse2019_get_discussion_data() : null; ?>

<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

<div class="entry-meta">
	<?php if ( ! is_page() ) : ?>
		<?php helsekompetanse2019_posted_by(); ?>
		<?php helsekompetanse2019_posted_on(); ?>
	<?php endif; ?>
	<?php if ( ! empty( $discussion ) ) : ?>
	<span class="comment-count">
		<?php helsekompetanse2019_discussion_avatars_list( $discussion->authors ); ?>
		<?php helsekompetanse2019_comment_count(); ?>
	</span>
	<?php endif; ?>
	<?php if ( ! is_page() ) : ?>
		<?php
		// Edit post link.
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers. */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'helsekompetanse2019' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					get_the_title()
				),
				'<span class="edit-link">' . helsekompetanse2019_get_icon_svg( 'edit', 16 ),
				'</span>'
			);
		?>
	<?php endif; ?>
</div><!-- .meta-info -->
