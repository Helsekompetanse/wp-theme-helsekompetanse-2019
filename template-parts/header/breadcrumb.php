<?php if ( helsekompetanse2019_breadcrumb_enabled() && is_page() && !is_front_page() ) : ?>
	<nav class="breadcrumb" aria-label="<?php esc_attr_e('breadcrumb', 'helsekompetanse2019'); ?>">
		<ul class="breadcrumbs">
			<li><a href="/"><?php _e('Front page', 'helsekompetanse2019'); ?></a></li><?php foreach ( array_reverse(get_ancestors( get_the_ID(), 'page' )) as $ancestor ) { ?><li><a href="<?php echo esc_url(get_page_link($ancestor)); ?>"><?php echo get_the_title($ancestor); ?></a></li><?php } ?><?php if ( is_singular() ) : ?><li><?php the_title(); ?></li><?php endif; ?>
		</ul>
	</nav>
<?php endif; ?>
