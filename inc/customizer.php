<?php
/**
 * Helsekompetanse 2019: Customizer
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.0.0
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function helsekompetanse2019_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'helsekompetanse2019_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'helsekompetanse2019_customize_partial_blogdescription',
			)
		);
	}

	/**
	 * Primary color.
	 */
	$wp_customize->add_setting(
		'primary_color',
		array(
			'default'           => 'default',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'helsekompetanse2019_sanitize_color_option',
		)
	);

	$wp_customize->add_control(
		'primary_color',
		array(
			'type'     => 'radio',
			'label'    => __( 'Primary Color', 'helsekompetanse2019' ),
			'choices'  => array(
				'default'  => _x( 'Default', 'primary color', 'helsekompetanse2019' ),
				'custom' => _x( 'Custom', 'primary color', 'helsekompetanse2019' ),
			),
			'section'  => 'colors',
			'priority' => 5,
		)
	);

	// Add primary color hue setting and control.
	$wp_customize->add_setting(
		'primary_color_hue',
		array(
			'default'           => 199,
			'transport'         => 'postMessage',
			'sanitize_callback' => 'absint',
		)
	);

	$wp_customize->add_control(
		new WP_Customize_Color_Control(
			$wp_customize,
			'primary_color_hue',
			array(
				'description' => __( 'Apply a custom color for buttons, links, featured images, etc.', 'helsekompetanse2019' ),
				'section'     => 'colors',
				'mode'        => 'hue',
			)
		)
	);

	// Add image filter setting and control.
	$wp_customize->add_setting(
		'image_filter',
		array(
			'default'           => 1,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'image_filter',
		array(
			'label'   => __( 'Apply a filter to featured images using the primary color', 'helsekompetanse2019' ),
			'section' => 'colors',
			'type'    => 'checkbox',
		)
	);

	// Add sans font option.
	$wp_customize->add_setting(
		'font_sans',
		array(
			'default'           => 0,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'font_sans',
		array(
			'label'   => __( 'Only use sans font on site.', 'helsekompetanse2019' ),
			'section' => 'title_tagline',
			'type'    => 'checkbox',
		)
	);

	// Add breadcrumb option.
	$wp_customize->add_setting(
		'breadcrumb',
		array(
			'default'           => 0,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'breadcrumb',
		array(
			'label'   => __( 'Activate breadcrumb on pages.', 'helsekompetanse2019' ),
			'section' => 'title_tagline',
			'type'    => 'checkbox',
		)
	);

	// Add option for white footer
	$wp_customize->add_setting(
		'white_footer',
		array(
			'default'           => 0,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'white_footer',
		array(
			'label'   => __( 'White footer background.', 'helsekompetanse2019' ),
			'section' => 'title_tagline',
			'type'    => 'checkbox',
		)
	);

	// Add option for left menu
	$wp_customize->add_setting(
		'left_local_menu',
		array(
			'default'           => 0,
			'sanitize_callback' => 'absint',
			'transport'         => 'postMessage',
		)
	);

	$wp_customize->add_control(
		'left_local_menu',
		array(
			'label'   => __( 'Left local menu.', 'helsekompetanse2019' ),
			'section' => 'title_tagline',
			'type'    => 'checkbox',
		)
	);
}
add_action( 'customize_register', 'helsekompetanse2019_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function helsekompetanse2019_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function helsekompetanse2019_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Bind JS handlers to instantly live-preview changes.
 */
function helsekompetanse2019_customize_preview_js() {
	wp_enqueue_script( 'helsekompetanse2019-customize-preview', get_theme_file_uri( '/js/customize-preview.js' ), array( 'customize-preview' ), '20181231', true );
}
add_action( 'customize_preview_init', 'helsekompetanse2019_customize_preview_js' );

/**
 * Load dynamic logic for the customizer controls area.
 */
function helsekompetanse2019_panels_js() {
	wp_enqueue_script( 'helsekompetanse2019-customize-controls', get_theme_file_uri( '/js/customize-controls.js' ), array(), '20181231', true );
}
add_action( 'customize_controls_enqueue_scripts', 'helsekompetanse2019_panels_js' );

/**
 * Sanitize custom color choice.
 *
 * @param string $choice Whether image filter is active.
 *
 * @return string
 */
function helsekompetanse2019_sanitize_color_option( $choice ) {
	$valid = array(
		'default',
		'custom',
	);

	if ( in_array( $choice, $valid, true ) ) {
		return $choice;
	}

	return 'default';
}
