<?php
/**
 * Helsekompetanse 2019 back compat functionality
 *
 * Prevents Helsekompetanse 2019 from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since Helsekompetanse 2019 1.0.0
 */

/**
 * Prevent switching to Helsekompetanse 2019 on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since Helsekompetanse 2019 1.0.0
 */
function helsekompetanse2019_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'helsekompetanse2019_upgrade_notice' );
}
add_action( 'after_switch_theme', 'helsekompetanse2019_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * Helsekompetanse 2019 on WordPress versions prior to 4.7.
 *
 * @since Helsekompetanse 2019 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function helsekompetanse2019_upgrade_notice() {
	$message = sprintf( __( 'Helsekompetanse 2019 requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'helsekompetanse2019' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since Helsekompetanse 2019 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function helsekompetanse2019_customize() {
	wp_die(
		sprintf(
			__( 'Helsekompetanse 2019 requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'helsekompetanse2019' ),
			$GLOBALS['wp_version']
		),
		'',
		array(
			'back_link' => true,
		)
	);
}
add_action( 'load-customize.php', 'helsekompetanse2019_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since Helsekompetanse 2019 1.0.0
 *
 * @global string $wp_version WordPress version.
 */
function helsekompetanse2019_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'Helsekompetanse 2019 requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'helsekompetanse2019' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'helsekompetanse2019_preview' );
