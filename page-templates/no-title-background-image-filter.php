<?php
/**
 * Template Name: No title—background image filter (featured image)
 *
 * The template for displaying single pages with fullscreen background.
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 2.1.5
 */

?><!doctype html>
<html <?php language_attributes(); ?> <?php body_class('page-no-title-fullscreen-background-filter'); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'helsekompetanse2019' ); ?></a>

	<div class="full-height wrapper">
		<header id="masthead" class="<?php echo is_singular() && helsekompetanse2019_can_show_post_thumbnail() ? 'site-header featured-image bg filter' : 'site-header'; ?>">

			<div class="site-branding-container">
				<?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
			</div><!-- .layout-wrap -->

			<?php if ( is_singular() && helsekompetanse2019_can_show_post_thumbnail() ) : ?>
				<div class="site-featured-image">
					<?php
						helsekompetanse2019_post_thumbnail();
						the_post();
						$discussion = helsekompetanse2019_can_show_post_thumbnail() ? helsekompetanse2019_get_discussion_data() : null;

						$classes = 'entry-header';
						if ( ! empty( $discussion ) && absint( $discussion->responses ) > 0 ) {
							$classes = 'entry-header has-discussion';
						}
					?>
					<?php if ( the_title('','',false) != '' || helsekompetanse2019_breadcrumb_enabled() && !is_front_page() ) : ?>
						<div class="<?php echo $classes; ?>">
							<?php get_template_part( 'template-parts/header/breadcrumb' ); ?>
							<?php if ( the_title('','',false) != '' ) : ?>
								<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
							<?php endif; ?>
						</div><!-- .entry-header -->
					<?php endif; ?>
					<?php rewind_posts(); ?>
				</div>
			<?php endif; ?>
		</header><!-- #masthead -->

	<div id="content" class="site-content">

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content/content', 'page' );

				// If local menu is set, prepare prev/next page buttons
				if ( has_nav_menu( 'local' ) || is_active_sidebar( 'sidebar-2' ) ) {
					get_template_part( 'template-parts/content/navigation/page', 'next-prev' );
				}

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
		</div><!-- .wrapper -->
	</div><!-- #content -->

	<?php get_template_part( 'template-parts/footer/footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
