<?php
/**
 * Template Name: Footnote page
 *
 * The template for displaying single footnote posts
 *
 * @package WordPress
 * @subpackage Helsekompetanse_2019
 * @since 1.4.5
 */

?><!doctype html>
<html <?php language_attributes(); ?> <?php body_class('page-footnote'); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
</head>

<body>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'helsekompetanse2019' ); ?></a>

	<header id="masthead" class="<?php echo is_singular() && helsekompetanse2019_can_show_post_thumbnail() ? 'site-header cover-title' : 'site-header'; ?>">

		<div class="site-branding-container">
			<div class="site-branding" hidden>
				<nav id="site-navigation" class="main-navigation" aria-label="Toppmeny">
					<div class="menu-primaer-container"><ul class="main-menu"><li class="menu-item"><a id="go-to-prev-page" href="/" ><span aria-hidden="true" class="font-serif font-weight-normal">← </span><?php _e('Back', 'helsekompetanse2019'); ?></a></li></ul></div>
				</nav>
			</div>
		</div><!-- .layout-wrap -->

		<?php if ( is_singular() && helsekompetanse2019_can_show_post_thumbnail() ) : ?>
			<div class="featured-image filter cover-title">
				<div class="site-featured-image">
					<?php
						helsekompetanse2019_post_thumbnail();
						the_post();
						$discussion = helsekompetanse2019_can_show_post_thumbnail() ? helsekompetanse2019_get_discussion_data() : null;

						$classes = 'entry-header';
						if ( ! empty( $discussion ) && absint( $discussion->responses ) > 0 ) {
							$classes = 'entry-header has-discussion';
						}
					?>
					<div class="<?php echo $classes; ?>">
						<?php get_template_part( 'template-parts/header/entry', 'header' ); ?>
					</div><!-- .entry-header -->
					<?php rewind_posts(); ?>
				</div>
			</div>
		<?php endif; ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">

		<div id="primary" class="content-area">
			<main id="main" class="site-main">

				<?php

				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					get_template_part( 'template-parts/content/content', 'footnote' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}

				endwhile; // End of the loop.
				?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<?php get_template_part( 'template-parts/footer/footer', 'site-info' ); ?>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php
	wp_footer();
?>

</body>
</html>
