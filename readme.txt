=== Helsekompetanse2019 ===
Contributors: Johan S. Steinberg
Tags: one-column, flexible-header, custom-colors, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, rtl-language-support, sticky-post, threaded-comments, translation-ready
Requires at least: 4.9.6
Tested up to: WordPress 5.3
Stable tag: 2.2.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This theme is based on the theme Twenty Nineteen. As Twenty Nineteen, it is especially made for the Gutenberg editor and otherwise designed to be used for learning and information sites.
